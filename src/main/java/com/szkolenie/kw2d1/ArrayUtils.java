package com.szkolenie.kw2d1;

public class ArrayUtils {
    public static int sum(int[] source) {
        int sum = 0;
        for (int x : source) {
            sum += x;
        }
        return sum;
    }

    public static int max(int[] source) {
        int max = source[0];
        for (int el : source) {
            if (el > max) {
                max = el;
            }
        }
        return max;
    }
}
