package com.szkolenie.kw2d1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilość elementów:");
        int size = sc.nextInt();
        int[] example = new int[size];

        for (int i = 0; i < example.length; i++) {
            System.out.println("Podaj element " + i);
            example[i] = sc.nextInt();
        }

        System.out.println("Suma: " + ArrayUtils.sum(example));
        System.out.println("Max: " + ArrayUtils.max(example));
    }
}
